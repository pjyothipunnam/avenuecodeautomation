import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class AutomationCode {

    public static void main(String args[]){


        System.setProperty("webdriver.chrome.driver","C:\\chromedriver\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://qa-test.avenuecode.com/");
        driver.findElement(By.xpath("//li/a[text()='Register']")).click();
        driver.findElement(By.id("user_name")).sendKeys("Jyothi");
        driver.findElement(By.id("user_email")).sendKeys("pjyothipunnam@gmail.com");
        driver.findElement(By.id("user_password")).sendKeys("TestAuto@Code");
        driver.findElement(By.id("user_password_confirmation")).sendKeys("TestAuto@Code");
        driver.findElement(By.name("commit")).click();
        driver.close();

    }

}
